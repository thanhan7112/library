package com.java.api.controllers;

import com.java.api.models.MenuModel;
import com.java.api.models.ResponseObject;
import com.java.api.repositories.MenuRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.beans.PropertyDescriptor;
import java.util.*;
import java.util.stream.Collectors;

//Báo cho java biết đây là một controller
@RestController
//Gửi request thông qua route bằng requestMapping tới controller ProductController
@RequestMapping(path = "api/menu")
@CrossOrigin(origins = "*")
public class MenuController {

    @Autowired
    private MenuRepository menuRepository;

    @Cacheable(value = "menus")
    @GetMapping("")
    public ResponseEntity<ResponseObject> getAllParents() {
        // Truy vấn tất cả các parent (không bao gồm các children riêng lẻ)
        List<MenuModel> parents = menuRepository.findAll().stream()
                .filter(menu -> Objects.isNull(menu.getParent()))
                .sorted(Comparator.comparingInt(MenuModel::getSort))
                .collect(Collectors.toList());

        // Sắp xếp children cho từng parent
        parents.forEach(MenuModel::sortChildren);

        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200, true, "Lấy dữ liệu thành công", parents)
        );
    }

    @GetMapping("/{id}")
    ResponseEntity<ResponseObject> findParentMenuById(@PathVariable Long id){
        Optional<MenuModel> foundParentMenu = menuRepository.findById(id);
        if(foundParentMenu.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200, true, "Lấy dữ liệu thành công", foundParentMenu)
            );
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseObject(404, false, "Không tìm thấy id = " + id, null)
        );
    }

//    GET children menu
    @GetMapping("/children/{parentId}")
    ResponseEntity<ResponseObject> findChildMenuById(@PathVariable Long parentId){
        List<MenuModel> foundChildMenu = menuRepository.findByParentId(parentId);
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200, true, "Lấy dữ liệu thành công", foundChildMenu)
        );
    }

//    Sử dụng @CacheEvict để xóa cache khi một mục bị thay đổi.
//    @CacheEvict(value = "menus", allEntries = true)
    @PostMapping("")
    ResponseEntity<ResponseObject> insertNewMenu(@RequestBody MenuModel newParentMenu){
        List<MenuModel> foundMenu = menuRepository.findByName(newParentMenu.getName().trim());
        if(!foundMenu.isEmpty()){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(
                    new ResponseObject(409, false, "Giá trị đã tồn tại", null)
            );
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(
            new ResponseObject(201, true, "Thêm mới thành công", menuRepository.save(newParentMenu))
        );
    }

    // Lấy thông tin một menu con dựa trên parentId và childId
    @GetMapping("/{parentId}/children/{childId}")
    public ResponseEntity<ResponseObject> getChildByParent(
            @PathVariable Long parentId,
            @PathVariable Long childId
    ) {
        // Tìm menu cha
        Optional<MenuModel> parentOptional = menuRepository.findById(parentId);

        if (parentOptional.isPresent()) {
            MenuModel parentMenu = parentOptional.get();

            // Tìm menu con trong danh sách children của menu cha
            Optional<MenuModel> childOptional = parentMenu.getChildren().stream()
                    .filter(child -> child.getId().equals(childId))
                    .findFirst();

            //                return ResponseEntity.ok(childOptional.get());
            return childOptional.map(menuModel -> ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200, true, "Lấy dữ liệu thành công", menuModel)
            )).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(400, false, "Không tìm thấy menu con", null)
            ));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(400, false, "Không tìm thấy menu cha", null)
            );
        }
    }

    // Thêm menu con vào menu cha
//    @CacheEvict(value = "menus", allEntries = true)
    @PostMapping("/{parentId}/children")
    public ResponseEntity<?> addChild(@PathVariable Long parentId, @RequestBody MenuModel newChild) {
            Optional<MenuModel> parentMenuOptional = menuRepository.findById(parentId);

        if (parentMenuOptional.isPresent()) {
            MenuModel parentMenu = parentMenuOptional.get();
            boolean childExists = parentMenu.getChildren().stream()
                    .anyMatch(child -> child.getName().equals(newChild.getName()));
            if (childExists) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(
                        new ResponseObject(409, false, "Menu con đã tồn tại", null)
                );
            }
            parentMenu.addChild(newChild);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(201, true, "Thêm mới menu con thành công", menuRepository.save(parentMenu))
            );
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, false, "Menu không tồn tại", null)
            );
        }
    }

//    @GetMapping("/{menuId}/article/{articleId}")
//    public ResponseEntity<ResponseObject> getArticleByParentMenu(
//            @PathVariable Long menuId,
//            @PathVariable Long articleId
//    ) {
//        // Tìm menu cha
//        Optional<MenuModel> parentOptional = menuRepository.findById(menuId);
//
//        if (parentOptional.isPresent()) {
//            MenuModel parentMenu = parentOptional.get();
//
//            // Tìm danh sách articles của menu cha
//            Optional<ArticleModel> articleOptional = parentMenu.getArticles().stream()
//                    .filter(article -> article.getId().equals(articleId))
//                    .findFirst();
//
//            return articleOptional.map(article -> ResponseEntity.status(HttpStatus.OK).body(
//                    new ResponseObject(200, true, "Lấy dữ liệu thành công", article)
//            )).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(
//                    new ResponseObject(400, false, "Không tìm thấy dữ liệu", null)
//            ));
//        } else {
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
//                    new ResponseObject(400, false, "Không tìm thấy menu cha", null)
//            );
//        }
//    }

//    @PostMapping("/{menuId}/article")
//    public ResponseEntity<?> addArticle(@PathVariable Long menuId, @RequestBody ArticleModel newArticle) {
//        Optional<MenuModel> parentMenuOptional = menuRepository.findById(menuId);
//
//        if (parentMenuOptional.isPresent()) {
//            MenuModel parentMenu = parentMenuOptional.get();
//            parentMenu.addArticle(newArticle);
//            return ResponseEntity.status(HttpStatus.OK).body(
//                    new ResponseObject(201, true, "Thêm mới bài viết thành công", menuRepository.save(parentMenu))
//            );
//        } else {
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
//                    new ResponseObject(404, false, "Menu không tồn tại", null)
//            );
//        }
//    }

    // Xóa menu con từ menu cha
//    @CacheEvict(value = "menus", allEntries = true)
    @DeleteMapping("/{parentId}/children/{childId}")
    public ResponseEntity<ResponseObject> deleteChild(@PathVariable Long parentId, @PathVariable Long childId) {
        Optional<MenuModel> parentOptional = menuRepository.findById(parentId);
        if (parentOptional.isPresent()) {
            MenuModel parent = parentOptional.get();
            Optional<MenuModel> childOptional = menuRepository.findById(childId);

            if (childOptional.isPresent() && parent.getChildren().contains(childOptional.get())) {
                MenuModel child = childOptional.get();
                parent.removeChild(child);
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseObject(204, true, "Xóa menu con thành công", menuRepository.save(parent))
                );
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject(404, false, "Menu con không tồn tại", null)
                );
            }
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, false, "Menu không tồn tại", null)
            );
        }
    }

//    Sử dụng @CachePut để cập nhật lại cache sau khi dữ liệu đã thay đổi.
//Xóa tất cả các mục trong cache (allEntries = true):
//@CacheEvict(value = "menus", allEntries = true)
    @PutMapping("/{id}")
    public ResponseEntity<ResponseObject> updateParentMenu(
            @RequestBody MenuModel updateParentMenu,
            @PathVariable Long id
    ) {
        try {
            Optional<MenuModel> foundMenu = menuRepository.findById(id);

            if (foundMenu.isPresent()) {
                MenuModel existingParent = foundMenu.get();

                // Sao chép các thuộc tính không null từ updateParentMenu sang existingParent
                copyNonNullProperties(updateParentMenu, existingParent);

                menuRepository.save(existingParent);

                return ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseObject(200, true, "Cập nhật menu thành công", existingParent)
                );
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject(404, false, "Không tìm thấy menu " + id, null)
                );
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject(500, false, "Lỗi khi cập nhật menu: " + e.getMessage(), null)
            );
        }
    }

    // Phương thức sao chép các thuộc tính không null
    private void copyNonNullProperties(Object source, Object target) {
        BeanWrapper src = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for (PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
        String[] result = new String[emptyNames.size()];
        emptyNames.toArray(result);

        BeanUtils.copyProperties(source, target, result);
    }

    // Phương thức cập nhật menu con
    private void updateChildren(Set<MenuModel> updatedChildren, MenuModel parent) {
        Set<MenuModel> existingChildren = parent.getChildren();
        List<MenuModel> childrenToRemove = new ArrayList<>(existingChildren);

        for (MenuModel updatedChild : updatedChildren) {
            // Kiểm tra sự tồn tại của menu con trong danh sách hiện tại
            MenuModel existingChild = existingChildren.stream()
                    .filter(c -> Objects.equals(c.getId(), updatedChild.getId()))
                    .findFirst()
                    .orElse(null);

            if (existingChild != null) {
                // Cập nhật thuộc tính của menu con từ đối tượng được gửi lên từ yêu cầu
                existingChild.setName(updatedChild.getName());
                existingChild.setIcon(updatedChild.getIcon());
                existingChild.setSort(updatedChild.getSort());
                existingChild.setPermission(updatedChild.getPermission());
                existingChild.setPermissionContext(updatedChild.getPermissionContext());
                existingChild.setUrl(updatedChild.getUrl());
                existingChild.setType(updatedChild.getType());
                existingChild.setTypeMenu(updatedChild.getTypeMenu());

                // Loại bỏ menu con này khỏi danh sách cần xóa
                childrenToRemove.remove(existingChild);
            } else {
                // Thêm menu con mới vào danh sách của menu cha
                parent.addChild(updatedChild);
            }
        }

        // Xóa các menu con không còn trong danh sách được gửi lên từ yêu cầu
        for (MenuModel childToRemove : childrenToRemove) {
            parent.removeChild(childToRemove);
        }
    }


    //    DELETE
//    Với cấu hình CascadeType.ALL, tất cả các "child" liên quan cũng sẽ bị xóa.
//    @CacheEvict(value = "menus", allEntries = true)
//    Xóa/Cập nhật một mục cụ thể trong cache bằng khóa (key)
//    @CachePut(value = "menus", key = "#id")
    @DeleteMapping("/{id}")
    ResponseEntity<ResponseObject> deleteParentMenu (@PathVariable Long id){
        boolean exist = menuRepository.existsById(id);
        if(exist){
            menuRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200, true, "Xóa thành công", null)
            );
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseObject(404, false, "Không tồn tại menu có id = " + id, null)
        );
    }
}
