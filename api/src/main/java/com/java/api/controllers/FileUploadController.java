package com.java.api.controllers;

import com.java.api.models.FileInfoModel;
import com.java.api.models.ResponseObject;
import com.java.api.repositories.ImageRepository;
import com.java.api.services.IStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "api/files")
@CrossOrigin(origins = "*")
public class FileUploadController {
    @Autowired
    private IStorageService storageService;
    @Autowired
    private ImageRepository imageRepository;

    @PostMapping("/multiple")
    public ResponseEntity<ResponseObject> uploadFiles(@RequestParam("files") MultipartFile[] files) {
        try {
            List<FileInfoModel> fileInfoList = new ArrayList<>();
            for (MultipartFile file : files) {
                if(!file.isEmpty()){
                    String generatedFileName = storageService.storageFiles(file);
                    String filePath = "/api/files/" + generatedFileName;

                    FileInfoModel fileInfo = new FileInfoModel(
                            generatedFileName,
                            file.getOriginalFilename(),
                            filePath,
                            file.getSize(),
                            file.getContentType()
                    );
                    fileInfoList.add(0, fileInfo);
                    imageRepository.save(fileInfo);
                }
            }
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200, true, "Tải lên thành công", fileInfoList)
            );
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject(500, false, "Tải lên thất bại", null)
            );
        }
    }

    @GetMapping("/{fileKey:.+}")
    public ResponseEntity<byte[]> readDetailFile(@PathVariable String fileKey) {
        try {
            // Đường dẫn tệp gốc
            Path originalFilePath = storageService.getFilePath(fileKey);

            // Tạo tên tệp mới (bản sao)
            String copiedFileKey = "copy_" + fileKey;
            Path copiedFilePath = storageService.getFilePath(copiedFileKey);

            // Sao chép tệp gốc sang tệp mới
            Files.copy(originalFilePath, copiedFilePath, StandardCopyOption.REPLACE_EXISTING);

            // Đọc nội dung từ tệp mới
            byte[] bytes = Files.readAllBytes(copiedFilePath);

            // Trả về nội dung của tệp đã sao chép
            ResponseEntity<byte[]> response = ResponseEntity.ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .body(bytes);

            // Xóa tệp đã sao chép sau khi trả về (để tiết kiệm không gian lưu trữ)
            Files.delete(copiedFilePath);

            return response;
        } catch (Exception exception) {
            // Xử lý lỗi khi không tìm thấy tệp hoặc không thể sao chép
            return ResponseEntity.noContent().build();
        }
    }
    @GetMapping("")
    public ResponseEntity<ResponseObject> readFiles(){
        try {
            // Lấy tất cả các đường dẫn tệp
            List<FileInfoModel> allFiles = imageRepository.findAll();

            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200, true, "Lấy danh sách thành công", allFiles)
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject(500, false, "Không thể lấy danh sách tệp", null)
            );
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<ResponseObject> deleteFiles(@RequestBody List<FileInfoModel> files) {
        try {
            for (FileInfoModel file : files) {
                try {
                    // Xóa tệp từ hệ thống lưu trữ
                    storageService.deleteFile(file.getFileKey());
                } catch (Exception e) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                            new ResponseObject(400, false, "Lỗi khi xóa tệp từ hệ thống lưu trữ: " + e.getMessage(), null)
                    );
                }
                try {
                    // Xóa thông tin tệp từ cơ sở dữ liệu
                    imageRepository.deleteById(file.getId());
                } catch (Exception e) {
//                    System.err.println("Lỗi khi xóa thông tin tệp khỏi cơ sở dữ liệu: " + e.getMessage());
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                            new ResponseObject(400, false, "Lỗi khi xóa thông tin tệp khỏi cơ sở dữ liệu: " + e.getMessage(), null)
                    );
                }
            }

            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200, true, "Xóa thành công", null)
            );
        } catch (Exception exception) {
            System.err.println("Lỗi khi xóa tệp: " + exception.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject(500, false, "Xóa thất bại: " + exception, null)
            );
        }
    }

}
