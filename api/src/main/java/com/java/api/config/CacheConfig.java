package com.java.api.config;

import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.ResourcePools;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.jsr107.EhcacheCachingProvider;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.CacheManager;
import javax.cache.Caching;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableCaching
public class CacheConfig {

    @Bean
    public JCacheCacheManager jCacheCacheManager() {
        return new JCacheCacheManager(cacheManager());
    }

    @Bean(destroyMethod = "close")
    public CacheManager cacheManager() {
        // Định nghĩa cấu hình resource pools cho các cache
        ResourcePools resourcePools = ResourcePoolsBuilder.newResourcePoolsBuilder()
                .heap(2000, EntryUnit.ENTRIES) //Xác định số lượng mục tối đa trong cache nằm trên heap.
                // Giá trị 2000 có nghĩa là cache có thể chứa tối đa 2000 mục trên heap.
                .offheap(100, MemoryUnit.MB) //Cho phép sử dụng bộ nhớ ngoài heap với giới hạn 100 MB.
                .build();

        // Định nghĩa các cấu hình cache khác nhau
        CacheConfiguration<Object, Object> menusCacheConfiguration =
                CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class, resourcePools)
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofMinutes(10))) // Đặt TTL (Thời gian quá hạn) 10 phút
                        .build();

        CacheConfiguration<Object, Object> articlesCacheConfiguration =
                CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class, resourcePools).build();

        // Tạo bản đồ chứa các cache với các tên khác nhau
        Map<String, CacheConfiguration<?, ?>> caches = new HashMap<>();
        caches.put("menus", menusCacheConfiguration); // Định nghĩa cache "menus"
        caches.put("articles", articlesCacheConfiguration); // Định nghĩa cache "articles"

        // Tạo CacheManager với các cấu hình này
        EhcacheCachingProvider provider = (EhcacheCachingProvider) Caching.getCachingProvider("org.ehcache.jsr107.EhcacheCachingProvider");
        org.ehcache.config.Configuration configuration = new org.ehcache.core.config.DefaultConfiguration(
                caches,
                provider.getDefaultClassLoader()
        );

        return provider.getCacheManager(provider.getDefaultURI(), configuration);
    }
}
