package com.java.api.services;

import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class ImageStorageService implements IStorageService{

    private final Path storageFolder = Paths.get("uploads");

//    Contructor
//    Gọi 1 lần duy nhất khi service hoặc app được chạy.
//    Tạo folder uploads
    public ImageStorageService(){
        try{
            Files.createDirectories(storageFolder);
        }catch (IOException exception){
            throw new RuntimeException("Không tìm thấy thư mục", exception);
        }
    }

    private boolean isImageFile(MultipartFile file){
        String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
//        Chi chap nhan image file ("png", "jpg", "jpeg", "bmp")
        assert fileExtension != null;
        return Arrays.asList(new String[] {"png", "jpg", "jpeg", "bmp"}).contains(fileExtension.trim().toLowerCase());
    }

    @Override
    public String storageFiles(MultipartFile file) {
        try{
            if(file.isEmpty()){
                throw new RuntimeException("Lỗi file trống");
            }
            if(!isImageFile(file)){
                throw new RuntimeException("Phải là file ảnh");
            }
            float fileSizeInMegabytes = (float) file.getSize() / 1000000;
            if(fileSizeInMegabytes > 5.0f){
                throw new RuntimeException("File không được vượt quá 5MB");
            }
//            File phải được đổi tên, để tránh trùng tên file
            String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
            String generatedFileName = UUID.randomUUID().toString().replace("-", "");
            generatedFileName = generatedFileName+"."+fileExtension;
            Path destinationFilePath = this.storageFolder.resolve(Paths.get(generatedFileName)).normalize().toAbsolutePath();
            if(!destinationFilePath.getParent().equals(this.storageFolder.toAbsolutePath())){
                throw new RuntimeException("không thể lưu trữ tập tin bên ngoài sự xúc phạm hiện tại");
            }
            try (InputStream inputStream = file.getInputStream()){
                Files.copy(inputStream, destinationFilePath, StandardCopyOption.REPLACE_EXISTING);
            }
            return generatedFileName;
        }catch (IOException exception) {
            throw new RuntimeException("storageFiles lỗi", exception);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        return Stream.empty();
    }

    @Override
    public byte[] readFileContent(String fileKey) {
        try{
            Path file = storageFolder.resolve(fileKey);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()){
                return StreamUtils.copyToByteArray(resource.getInputStream());
            }else{
                throw new RuntimeException("Không thể đọc file: " + fileKey);
            }
        }catch (IOException e){
            throw new RuntimeException("Không thể đọc file: " + fileKey);
        }
    }

    @Override
    public void deleteAllFiles() {

    }
    @Override
    public void deleteFile(String fileKey) {
        try {
            Path fileToDelete = storageFolder.resolve(fileKey).normalize();

            if (!Files.exists(fileToDelete)) {
                throw new RuntimeException("Tệp không tồn tại: " + fileKey); // Tệp không tồn tại
            }

            Path tempFileToDelete = Files.createTempFile(null, null);
            Files.copy(fileToDelete, tempFileToDelete, StandardCopyOption.REPLACE_EXISTING);

            // Xóa tệp gốc
            Files.delete(fileToDelete);
            System.out.println("Xóa thành công: " + fileToDelete.toString());

        } catch (IOException e) {
            // Xử lý ngoại lệ và in thông báo lỗi
            System.err.println("Lỗi khi xóa tệp từ hệ thống lưu trữ: " + fileKey + ", lý do: " + e.getMessage());
            throw new RuntimeException("Lỗi khi xóa tệp: " + fileKey, e);
        }

    }

    @Override
    public Path getFilePath(String fileKey) {
        return storageFolder.resolve(fileKey).normalize().toAbsolutePath();
    }
}
