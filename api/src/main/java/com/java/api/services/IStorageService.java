package com.java.api.services;

import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface IStorageService {
    public String storageFiles(MultipartFile file);
    public Stream<Path> loadAll(); //Load toan bo file trong folder chua anh
    public byte[] readFileContent(String fileKey); //Mang byte de co the xem chi tiet file
    public void deleteAllFiles();
    public void deleteFile(String fileKey);
    public Path getFilePath(String fileKey);
}
