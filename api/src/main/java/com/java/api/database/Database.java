package com.java.api.database;

import com.java.api.models.MenuModel;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
docker run -d --rm --name mysql-spring-boot-tutorial -e MYSQL_ROOT_PASSWORD=0201172001An -e MYSQL_USER=root -e MYSQL_PASSWORD=0201172001An -e MYSQL_DATABASE=javadb -p 3306:3306 --volume mysql-spring-boot-tutorial-volume:/var/lib/mysql mysql:latest mysql -h localhost -P 3306 --protocol=tcp -u root -p
**/

// -p 3306:3306 => trước là port của docker, sau là mysql => docker link tới mysql

// Configuration giúp cho các Bean methods trong Database được gọi ngay khi ứng dụng chạy
@Configuration
public class Database {
    @Bean
    public MenuModel menuModel() {
        return new MenuModel();
    }

    @Bean
    CommandLineRunner initDatabase(MenuModel menuModel) {
        return args -> {
            // Code to initialize database or other operations
        };
    }

}
