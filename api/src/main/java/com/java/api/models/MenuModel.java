package com.java.api.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "tbMenu")
public class MenuModel implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L; // Thêm số phiên bản tuần tự hóa
    @Id
    @SequenceGenerator(
            name = "menu_sequence",
            sequenceName = "menu_sequence",
            allocationSize = 1
    )
    @GeneratedValue(generator = "menu_sequence", strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String permission;
    private String permissionContext;
    private Integer sort;
    private String icon;
    private String url;
    private Integer type;
    @Lob
    @Column(length = 1000000)
    private String content;
    // Sử dụng Enum cho trường type
    @Enumerated(EnumType.STRING)
    private TypeMenu typeMenu;

//    @JsonBackReference
    @ManyToOne
    @JsonBackReference
    private MenuModel parent;

    // Quan hệ một-đến-nhiều với các tùy chọn cascade và orphanRemoval
    @JsonManagedReference
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<MenuModel> children;

    public MenuModel() {}

    public MenuModel(String name, String content, String permission, String permissionContext, Integer sort, String icon, String url, Integer type, TypeMenu typeMenu, MenuModel parent, Set<MenuModel> children) {
        this.name = name;
        this.permission = permission;
        this.permissionContext = permissionContext;
        this.sort = sort;
        this.icon = icon;
        this.url = url;
        this.type = type;
        this.typeMenu = typeMenu;
        this.parent = parent;
        this.children = children;
        this.content = content;
    }

    public void sortChildren() {
        if (children != null) {
            children = children.stream()
                    .sorted(Comparator.comparingInt(MenuModel::getSort))
                    .collect(Collectors.toCollection(LinkedHashSet::new));
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getPermissionContext() {
        return permissionContext;
    }

    public void setPermissionContext(String permissionContext) {
        this.permissionContext = permissionContext;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public TypeMenu getTypeMenu() {
        return typeMenu;
    }

    public void setTypeMenu(TypeMenu typeMenu) {
        this.typeMenu = typeMenu;
    }

    public MenuModel getParent() {
        return parent;
    }

    public void setParent(MenuModel parent) {
        this.parent = parent;
    }

    public Set<MenuModel> getChildren() {
        return children;
    }

    public void setChildren(Set<MenuModel> children) {
        this.children = children;
    }

    // Phương thức thêm child
    public void addChild(MenuModel child) {
        if (child == null || child == this) {
            throw new IllegalArgumentException("Giá trị không tồn tại.");
        }
        if (!children.contains(child)) {  // Đảm bảo không thêm trùng lặp
            children.add(child);
            child.setParent(this);  // Đặt parent khi thêm child
        }
    }

    // Phương thức xoá child
    public void removeChild(MenuModel child) {
        children.remove(child);
        child.setParent(null);
    }


    public enum TypeMenu {
        MENU,
        ARTICLE
    }

}
