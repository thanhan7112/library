package com.java.api.models;

import jakarta.persistence.*;

@Entity
@Table(name = "tbImages")

public class FileInfoModel {
    @Id
    @SequenceGenerator(
            name = "image_sequence",
            sequenceName = "article_sequence",
            allocationSize = 1
    )
    @GeneratedValue(generator = "article_sequence", strategy = GenerationType.AUTO)
    private Long id;
    private String fileKey;
    private String fileName;
    private String filePath;
    private Number fileSize;
    private String fileType;

    public FileInfoModel(){}

    public FileInfoModel(String fileKey, String fileName, String filePath, Number fileSize, String fileType) {
        this.fileKey = fileKey;
        this.fileName = fileName;
        this.filePath = filePath;
        this.fileSize = fileSize;
        this.fileType = fileType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Number getFileSize() {
        return fileSize;
    }

    public void setFileSize(Number fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
