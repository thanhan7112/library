package com.java.api.models;

// Định dạng cấu trúc kết quả trả về (response)
public class ResponseObject {
    private Number status;
    private String message;
    private Object result;
    private Boolean success;

//    Contructor default
    public ResponseObject () {}

    public ResponseObject(Number status, Boolean success, String message, Object result) {
        this.status = status;
        this.message = message;
        this.result = result;
        this.success = success;
    }

    public Number getStatus() {
        return status;
    }

    public void setStatus(Number status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
