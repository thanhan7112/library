package com.java.api.repositories;

import com.java.api.models.FileInfoModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<FileInfoModel, Long> {
}
