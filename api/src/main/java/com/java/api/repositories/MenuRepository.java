package com.java.api.repositories;

import com.java.api.models.MenuModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MenuRepository extends JpaRepository<MenuModel, Long> {
    List<MenuModel> findByName(String title);
    List<MenuModel> findByParentId(Long parentId);
}
